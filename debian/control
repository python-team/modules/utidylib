Source: utidylib
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Alexandre Detiste <tchet@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               libtidy58,
               python3-all,
               python3-setuptools
Rules-Requires-Root: no
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/utidylib
Vcs-Git: https://salsa.debian.org/python-team/packages/utidylib.git
Homepage: https://cihar.com/software/utidylib/

Package: python3-utidylib
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         libtidy58,
Breaks: ${python3:Breaks}
Provides: ${python3:Provides}
Description: Python wrapper for TidyLib
 Corrects markup in a way compliant with the latest standards, and
 optimal for the popular browsers.  It has a comprehensive knowledge
 of the attributes defined in the HTML 4.0 recommendation from W3C,
 and understands the US ASCII, ISO Latin-1, UTF-8 and the ISO 2022
 family of 7-bit encodings.  In the output:
 .
  * HTML entity names for characters are used when appropriate.
  * Missing attribute quotes are added, and mismatched quotes found.
  * Tags lacking a terminating '>' are spotted.
  * Proprietary elements are recognized and reported as such.
  * The page is reformatted, from a choice of indentation styles.
 .
 This package contains uTidylib, a Python 3 wrapper for TidyLib.
